import { Message, RichEmbed, RichEmbedOptions } from "discord.js";

interface DefaultEmbedOptions extends RichEmbedOptions {
	pure?: boolean;
}

export const err = (
	m: Message,
	content: string,
	modifiers?: DefaultEmbedOptions,
) => {
	if (modifiers && modifiers.pure) {
		return new RichEmbed(modifiers)
			.setColor(0xff5555)
			.setDescription(":x: " + content);
	}
	return m.channel.send({
		embed: new RichEmbed(modifiers)
			.setColor(0xff5555)
			.setDescription(":x: " + content),
	});
};

export const check = (
	m: Message,
	content: string,
	modifiers?: DefaultEmbedOptions,
) => {
	if (modifiers && modifiers.pure) {
		return new RichEmbed(modifiers)
			.setColor(0x55ff55)
			.setDescription(":ballot_box_with_check: " + content);
	}
	return m.channel.send({
		embed: new RichEmbed(modifiers)
			.setColor(0x55ff55)
			.setDescription(":ballot_box_with_check: " + content),
	});
};
