// @ts-ignore
import * as juration from "juration";
import { Embed, PluginWithConfig, SparklClient } from "sparkldjs/src";

import { ChannelPlayer } from "../structures/ChannelPlayer";
import { ChannelRecorder } from "../structures/ChannelRecorder";
import { check, err } from "../util/DefaultEmbeds";

export class VCPlugin extends PluginWithConfig<{
	recorder?: ChannelRecorder;
	player?: ChannelPlayer;
}> {
	public constructor(client: SparklClient) {
		super(client);

		this.pluginName = "VCPlugin";
		this.config = {};
	}
	public async init() {
		/*
		this.client.discord.on("voiceStateUpdate", async (o, n) => {
			if (o.guild.id !== "360462032811851777") {
				return;
			}

			if (!o.serverMute && n.serverMute && this.config.enabled) {
				this.logger.log("[VC] Server mute detected.");
				const auditLogs = await o.guild.fetchAuditLogs({
					// @ts-ignore
					after: SnowflakeUtil.generate(Date.now() - 10e3),
				});

				const auditEntry = auditLogs.entries
					.filter(
						(v: GuildAuditLogsEntry) =>
							v.actionType === "UPDATE" &&
							(v.action === "MEMBER_UPDATE" &&
								(v.target as User).id) === n.id,
					)
					.first();

				if (auditEntry) {
					this.logger.log("[VC] Found a matching audit log entry.");
					const STAFF_CHAT = this.client.discord.channels.get(
						"417710433533689866",
					) as TextChannel;
					if (!STAFF_CHAT) {
						return;
					}

					STAFF_CHAT.send(
						`:information_source: <@${
							auditEntry.executor.id
						}>: **You server muted member <@${
							(auditEntry.target as User).id
						}> (\`${(auditEntry.target as User).id}\`).** ` +
							"Use `!vcgen [reason:string...]` to generate a Rawgoat infraction. Disable this message with `!vctoggle`.",
					);

					this.updateConfig({
						latestPunish: n.id,
					});
				}
			}
		});

		// Generation command
		this.client.command(
			"vcgen",
			0,
			"[reason:string...]",
			(c, m, a: [string]) => {
				if (m.channel.id !== "417710433533689866") {
					return;
				}
				if (!this.config.latestPunish) {
					return err(
						m,
						"VC punishment has already been made/does not exist.",
					);
				} else {
					m.channel.send(
						`\`!tempmute ${
							this.config.latestPunish
						} 30m [DM] ${a[0] || "No reason specified."}\``,
					);
					this.updateConfig({
						latestPunish: undefined,
					});
				}
			},
		);

		this.client.command("vctoggle", 0, "", (c, m, a) => {
			if (m.channel.id !== "417710433533689866") {
				return;
			}

			const prev = this.config.enabled;
			this.updateConfig({
				enabled: !this.config.enabled,
			});

			if (prev) {
				check(m, "Disabled server mute/audit log detection.");
			} else {
				check(m, "Enabled server mute/audit log detection.");
			}
		});
		*/

		this.client.command("vc.join", 10, "", (c, m, a) => {
			if (!m.member.voiceChannel) {
				return err(
					m,
					"You are not in a voice channel! Please join a voice channel in order to run this command.",
				);
			}
			if (!m.member.voiceChannel.joinable) {
				return err(
					m,
					`Lacking permission to join ${m.member.voiceChannel}.`,
				);
			}
			m.member.voiceChannel.join();
		});
		this.client.command("vc.leave", 10, "", async (c, m, a) => {
			if (!m.guild.voiceConnection) {
				return err(m, "Not currently in a voice channel.");
			} else {
				m.guild.voiceConnection.channel.leave();
			}
		});

		this.client.command("vc.record", 10, "", (c, m, a) => {
			if (!m.guild.voiceConnection) {
				return err(m, "Not currently in a voice channel.");
			}
			this.config = {
				recorder: new ChannelRecorder(c, m.guild.voiceConnection),
			};
			check(m, "Recording...");
		});

		this.client.command(
			"vc.play",
			10,
			"<url:string>",
			async (c, m, a: [string]) => {
				if (this.config.player) {
					const song = await this.config.player
						.addToQueue(a[0])
						.catch((e) => {
							err(m, `Could not add song to queue. \`${e}\``);
						});
					if (song) {
						check(m, "Added song to queue.");
					}
				} else {
					this.updateConfig({
						player: new ChannelPlayer(c, m.guild.voiceConnection),
					});
					if (this.config.player) {
						const song = await this.config.player
							.addToQueue(a[0])
							.catch((e) => {
								err(m, `Could not add song to queue. \`${e}\``);
							});
						if (song) {
							check(m, "Added song to queue.");
						}
					} else {
						err(m, "Failed to create player.");
					}
				}
			},
		);

		this.client.command("vc.queue", 10, "", (c, m, a) => {
			if (!m.guild.voiceConnection) {
				return err(m, "Not currently in a voice channel.");
			}
			if (this.config.player) {
				m.channel.send({
					embed: new Embed(c)
						.setTitle("Queue")
						.setDescription(
							this.config.player.queue
								.map(
									(v) =>
										`${v.author.name} - ${
											v.title
										} (\`${juration.stringify(
											parseInt(v.length_seconds, 10),
										)}\`)`,
								)
								.join("\n"),
						),
				});
			} else {
				return m.reply("There are no songs in the queue.");
			}
		});

		process.on("SIGINT", async () => {
			if (this.config.recorder) {
				this.config.recorder.connection.channel.leave();
			}
			const iterator = this.client.discord.voiceConnections.map(
				async (v) => {
					v.channel.leave();
				},
			);
			await Promise.all(iterator);
		});

		this.logger.log("[VC] Ready.");
	}
}
