import {
    Collection, GuildMember, Message, RichEmbed, SnowflakeUtil, TextChannel, User
} from "discord.js";
import { Plugin, SparklClient } from "sparkldjs/src";

import { check, err } from "../util/DefaultEmbeds";

export class UtilPlugin extends Plugin {
	constructor(client: SparklClient) {
		super(client);

		this.pluginName = "UtilPlugin";
	}
	public async init() {
		this.client.command(
			"id",
			0,
			"<memberToGrab:member|user>",
			(c, m, a: [GuildMember | User]) => {
				m.channel.send(a[0].id);
			},
		);
		this.logger.info("[UTIL] Ready.");

		this.client.command(
			"eval",
			100,
			"<code:string...>",
			(c, m, a: [string]) => {
				try {
					const start = new Date();
					const toEval = a[0].replace("\n", "");
					// tslint:disable-next-line: no-eval
					const res = eval(toEval);
					let end = new Date();

					if (res instanceof Promise) {
						m.reply(
							check(m, "Executing asyncronous evaluation..."),
						);
						res.then(
							(r) => {
								end = new Date();
								return m.channel.send(
									new RichEmbed()
										.setTitle("Evaluation Successful")
										.setDescription(
											`Began at: \`${start}\`
          	      Ended at: \`${end}\`\n
                Took: \`${end.getTime() - start.getTime()}ms\`\n
                \`\`\`${r}\`\`\``,
										)
										.setColor(0x70ff70),
								);
							},
							(ex) => {
								return m.channel.send(
									new RichEmbed()
										.setTitle("Evaluation Failed")
										.setDescription(`\`\`\`${ex}\`\`\``)
										.setColor(0xff7070),
								);
							},
						);
					} else {
						return m.channel.send(
							new RichEmbed()
								.setTitle("Evaluation Successful")
								.setDescription(
									`Took: \`${end.getTime() -
										start.getTime()}ms\`
                \`\`\`${res}\`\`\``,
								)
								.setColor(0x70ff70),
						);
					}
				} catch (err) {
					return m.channel.send(
						new RichEmbed()
							.setTitle("Evaluation Failed")
							.setDescription(`\`\`\`${err}\`\`\``)
							.setColor(0xff7070),
					);
				}
			},
		);

		this.client.command(
			"fetchreactions",
			30,
			"<channel:string> <id:string>",
			async (c, m, a: [string, string]) => {
				const CHANNEL = m.guild.channels.get(a[0]) as TextChannel;
				if (!CHANNEL) {
					return err(m, "Could not find channel.");
				}
				const msg: Message | void = await CHANNEL.fetchMessage(
					a[1],
				).catch(() => {
					err(m, "Could not find message.");
				});
				if (msg) {
					check(m, "Fetching reactions...");

					const START = parseInt(SnowflakeUtil.generate(), 10);
					const DECREMENT = Math.floor(
						START / msg.reactions.first().count / 10,
					);
					let AFTER_VALUE = START - DECREMENT;
					let BEFORE_VALUE = START;

					const reactions = new Collection();

					while (AFTER_VALUE > 0) {
						console.log(
							"Using AFTER",
							AFTER_VALUE,
							" - BEFORE",
							BEFORE_VALUE,
						);

						const r = await msg.reactions.first().fetchUsers(100, {
							after: AFTER_VALUE,
							before: BEFORE_VALUE,
						});
						console.log("got", r.size, "users");
						const iterator = r.map((v) =>
							reactions.set(v.id, v.tag),
						);
						await Promise.all(iterator);
						BEFORE_VALUE = AFTER_VALUE;
						AFTER_VALUE = AFTER_VALUE - DECREMENT;
					}

					check(m, `Fetched ${reactions.size} users.`);
				}
			},
		);
	}
}
