import { Message, MessageReaction, ReactionCollector, RichEmbed, TextChannel } from "discord.js";
import { PluginWithConfig, SparklClient } from "sparkldjs/src";

import { check, err } from "../util/DefaultEmbeds";

interface Raid {
	started: Date;
	totalBans: number;
}

interface RaidPluginConfig {
	timeout: NodeJS.Timeout | undefined;
	currentRaid: Raid | undefined;
	collector: ReactionCollector | undefined;
	raidStatMessage: Message | undefined;
}

export class RaidPlugin extends PluginWithConfig<RaidPluginConfig> {
	constructor(c: SparklClient) {
		super(c);
		this.pluginName = "RaidPlugin";
	}
	public async init() {
		this.client.command("raid", 0, "", async (c, m, a: [string]) => {
			if (m.deletable) {
				m.delete();
			}

			if (m.channel.id !== "417710433533689866") {
				return;
			}

			if (this.config.timeout) {
				(err(m, "Raid mode is already being enabled!") as Promise<
					Message
				>).then((msg) => {
					if (msg.deletable) {
						msg.delete(5e3);
					}
				});

				return;
			}

			if (this.config.currentRaid) {
				check(m, "Raid mode disabled.");

				const statEmbed = new RichEmbed()
					.setTitle("Raid Stats")
					.setDescription("Phew! That was fun hehe!")
					.addField("Started At", this.config.currentRaid.started)
					.addField("Total Bans", this.config.currentRaid.totalBans)
					.setColor(0x55ff55);

				this.updateConfig({
					currentRaid: undefined,
				});
				return m.channel.send({ embed: statEmbed });
			}

			const embed = new RichEmbed()
				.setDescription(
					":warning: **WARNING** - React below to engage raid mode. This will time out in 30 seconds.",
				)
				.setColor(0xff5555);

			const message = (await m.channel
				.send({ embed })
				.catch((s: any) => this.logger.error(s))) as Message;

			this.updateConfig({
				timeout: setTimeout(() => {
					message.edit(
						err(m, "Raid mode activation timed out.", {
							pure: true,
						}),
					);
					if (this.config.timeout) {
						clearTimeout(this.config.timeout);
						this.updateConfig({
							timeout: undefined,
						});
					}
				}, 30e3),
			});

			await message.react("✅");
			await message.react("❎");

			this.updateConfig({
				collector: message
					.createReactionCollector(
						(v: MessageReaction) =>
							v.emoji.name === "✅" || v.emoji.name === "❎",
					)
					.on("collect", async (v: MessageReaction) => {
						if (!v.users.has(m.author.id)) {
							return;
						}

						if (v.emoji.name === "❎") {
							message.edit(
								check(message, "Raid activation canceled.", {
									pure: true,
								}),
							);

							await message.clearReactions().catch(() => {
								c.logger.error(
									"Missing perms to bulk-remove reactions.",
								);
							});
							const iterator = message.reactions
								.filter((mr) => mr.me)
								.map(async (vr) => await vr.remove());
							await Promise.all(iterator);

							if (this.config.collector) {
								this.config.collector.stop();
							}

							if (this.config.timeout) {
								clearTimeout(this.config.timeout);
								this.updateConfig({
									timeout: undefined,
								});
							}
						} else {
							message.edit(
								check(m, "**RAID MODE ENALBED.**", {
									pure: true,
								}),
							);

							const ALL_MEMBERS = c.channels.get(
								"360462032811851778",
							) as TextChannel;
							if (ALL_MEMBERS) {
								const ALL_MEMBERS_EMBED = new RichEmbed()
									.setTitle(
										":warning: **WARNING** - owo what's this? **A RAID?** *HEHEHE*",
									)
									.setDescription(
										"Nobody has been banned yet. Hopefully that'll change uwu.",
									)
									.setColor(0xff5555);

								this.updateConfig({
									raidStatMessage: (await ALL_MEMBERS.send({
										embed: ALL_MEMBERS_EMBED,
									})) as Message,
								});
							}

							this.updateConfig({
								currentRaid: {
									started: new Date(),
									totalBans: 0,
								},
							});

							await message.clearReactions().catch(() => {
								c.logger.error(
									"Missing perms to bulk-remove reactions.",
								);
							});
							const iterator = message.reactions
								.filter((mr) => mr.me)
								.map(async (vr) => await vr.remove());
							await Promise.all(iterator);

							if (this.config.collector) {
								this.config.collector.stop();
							}

							if (this.config.timeout) {
								clearTimeout(this.config.timeout);
								this.updateConfig({
									timeout: undefined,
								});
							}
						}
					}),
			});
		});
		this.logger.info("[RAID] Ready.");
	}
}
