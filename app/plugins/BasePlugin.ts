import { Message } from "discord.js";
import * as mongoose from "mongoose";
import { PluginWithConfig, SparklClient } from "sparkldjs/src";

interface BasePluginConfig {
	ready: boolean;
}

function setPresence(c: SparklClient) {
	c.user.setActivity("gamesgamesgames 🎉");
}
export class BasePlugin extends PluginWithConfig<BasePluginConfig> {
	public constructor(client: SparklClient) {
		super(client);

		this.pluginName = "BasePlugin";
	}
	public async init() {
		console.log("boop");

		setPresence(this.client);
		this.on("resume", () => setPresence(this.client));

		this.on("message", (m: Message) => {
			if (!m.guild && m.author.id !== this.client.user.id) {
				this.client.logger.info(
					`[DM] ${m.author.tag} (${m.author.id}) | ${m.cleanContent}`,
				);
			}
		});

		this.client.config.set("360462032811851777", "permissions", {
			roles: {
				// mod staff
				"360463652429496330": 10,
				// admins
				"366255103642173441": 20,
				// owners
				"431843721986703391": 40,
				// lt
				"528712072318550036": 30,
			},
			users: {
				"156670353282695168": 20,
				"210118905006522369": 100,
			},
		});

		this.client.config.set("360462032811851777", "prefix", "s!");

		mongoose
			.connect("mongodb://localhost:27017/sfe-games", {
				useNewUrlParser: true,
			})
			.then(
				() => this.logger.info("[BASE] Connected to MongoDB."),
				(err) => {
					this.logger.error(
						"[BASE] Could not connect to MongoDB: " + err.message,
					);
					process.exit();
				},
			);

		this.logger.info("[BASE] Ready.");
	}
}
