import { GuildMember, Message } from "discord.js";
import { PluginWithConfig, SparklClient } from "sparkldjs/src";

type ActionType = "NUZZLE" | "SNUGGLE" | "CUDDLE" | "PAT" | "HUG";
const ACTIONS: Array<[ActionType, string]> = [
	["NUZZLE", "nuzzled"],
	["SNUGGLE", "snuggled"],
	["CUDDLE", "cuddled"],
	["PAT", "patted"],
	["HUG", "hugged"],
];

interface FurPluginConfig {
	counts: {
		[x: string]: { [key in ActionType]: number };
	};
}

function generateFurry() {
	const possibleDegeneracy = ["owo", "uwu", "o3o", "^w^", ":3", "nyeh"];
	let toReturn =
		possibleDegeneracy[
			Math.floor(Math.random() * possibleDegeneracy.length)
		];

	let latestValue = toReturn;

	for (let i = 0; i < possibleDegeneracy.length; i++) {
		if (Math.random() < 0.5 / 2 ** i) {
			latestValue = possibleDegeneracy.splice(
				possibleDegeneracy.indexOf(latestValue),
				1,
			)[0];
			toReturn += ` ${
				possibleDegeneracy[
					Math.floor(Math.random() * possibleDegeneracy.length)
				]
			}`;
		}
	}

	return toReturn;
}

export class FurPlugin extends PluginWithConfig<FurPluginConfig> {
	constructor(client: SparklClient) {
		super(client);

		this.pluginName = "FurPlugin";

		this.config = {
			counts: {},
		};
	}
	public async init() {
		ACTIONS.map((v) =>
			this.client.command(
				v[0].toLowerCase(),
				0,
				`<memberToBe${v[1].charAt(0).toUpperCase() +
					v[1].slice(1)}:member>`,
				(c, m, a: [GuildMember]) => {
					if (m.deletable) {
						m.delete();
					}
					m.channel.send(
						Math.random() > 0.5
							? `${m.member} gave ${
									a[0]
							  } a ${v[0].toLowerCase()} ${generateFurry()}`
							: `${m.member} ${v[1]} ${
									a[0]
							  } ${generateFurry()} ${generateFurry()}`,
					);
				},
			),
		);

		this.on("message", (m: Message) => {
			if (
				m.mentions.users.first() &&
				m.mentions.users.first().id === this.client.user.id
			) {
				const content = m.content.split(" ")[1];
				if (content) {
					let verb = content.split("*")[1];
					if (verb) {
						if (verb.endsWith("s")) {
							verb = verb.slice(0, -1);
						}
						if (
							ACTIONS.map((v) => v[0].toLowerCase()).indexOf(
								verb,
							) !== -1
						) {
							m.channel.send(":3");
						}
					}
				}
			}

			if (
				m.author.id === "382197942762602496" &&
				m.cleanContent.startsWith("Well done") &&
				m.mentions.members.find(
					(v) =>
						["210118905006522369", "332209233577771008"].indexOf(
							m.mentions.members.first().id,
						) !== -1,
				)
			) {
				m.channel.send(
					`<@${
						m.mentions.members.first().id
					}> ur such a guud boi *patpat* :3`,
				);
			}
		});

		this.logger.info("[FUR] Ready.");
	}
}
