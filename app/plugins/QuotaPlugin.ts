import { Collection, GuildMember, Message, Role } from "discord.js";
import { unlinkSync, writeFileSync } from "fs";
// import { existsSync, mkdirSync, writeFileSync } from "fs";
// @ts-ignore
import * as juration from "juration";
import { Embed, PluginWithConfig, SparklClient } from "sparkldjs/src";

import { QuotaUserModel } from "../../shared/models/QuotaUserModel";
import { QuotaUser } from "../../shared/structures/QuotaUser";
import { VCQuotaSession } from "../structures/VCQuotaSession";
import { check, err } from "../util/DefaultEmbeds";
import { QuotaWeek } from "../util/Util";

export class QuotaPlugin extends PluginWithConfig<{
	activeVoiceSessions: Collection<string, VCQuotaSession>;
	dmExceptions: string[];
}> {
	public constructor(client: SparklClient) {
		super(client);

		this.pluginName = "QuotaPlugin";
		this.config = {
			activeVoiceSessions: new Collection(),
			dmExceptions: [],
		};
	}
	public async init() {
		// Voice handler - check for staff in VC on voiceStateUpdate
		this.on("voiceStateUpdate", (o: GuildMember, n: GuildMember) =>
			this.checkForStaffInVC(o, n),
		);

		// Check for staff 5 seconds after bot starts - wait for client.guilds to be hydrated
		setTimeout(() => this.checkForStaffInVC(), 5e3);

		// Command for enabling/disabling notifications.
		this.client.command("quota.toggle", 10, "", (c, m, a) => {
			if (this.config.dmExceptions.indexOf(m.author.id) !== -1) {
				this.config.dmExceptions.splice(
					this.config.dmExceptions.indexOf(m.author.id),
					1,
				);
				return check(m, "Enabled VC quota DMs.");
			}
			this.config.dmExceptions.push(m.author.id);
			return check(m, "Disabled staff member VC quota DMs.");
		});

		// Force VC check
		this.client.command("quota.force", 40, "", (c, m, a) => {
			this.checkForStaffInVC();
		});

		// List current staff in VC
		this.client.command("quota.list", 30, "", (c, m, a) => {
			const activeSessions = this.config.activeVoiceSessions
				.map(
					(v) =>
						` - ${v.member.user.tag} (\`${
							v.member.id
						}\`) - \`${juration.stringify(
							Math.floor(v.length / 1000),
						)}\` - ${v.messages} equivalent`,
				)
				.join("\n");

			const embed = new Embed(this.client)
				.setTitle("VC Quota")
				.setDescription(
					"Here are a list of all active staff VC sessions:\n\n" +
						(activeSessions.length > 1
							? activeSessions
							: " - No active sessions") +
						"\n\nFormat: tag (`id`) - `length` - messages",
				);
			m.channel.send({ embed });
		});

		// Check current quota for executor
		this.client.command("quota", 10, "", async (c, m, a) => {
			const amount = await this.getEntry(m.author.id);

			if (!amount) {
				return err(
					m,
					"You currently have no data for this week. Send some messages or join a VC to start!",
				);
			}

			const vc = this.config.activeVoiceSessions.get(m.author.id);

			const embed = new Embed(this.client)
				.setTitle(`Quota Progress for ${m.author.tag}`)
				.setDescription(
					`${
						this.config.activeVoiceSessions.get(m.author.id)
							? "You are currently in a VC. Below values are approximations.\n\n"
							: ""
					}❯ **Voice Channel Quota**
VC Messages: \`${vc ? amount.vc + vc.messages : amount.vc}\`
Equivalent Time: \`${juration.stringify(
						(amount.vc / 50) * 60 ** 2 +
							(vc ? Math.floor(vc.length / 1000) : 0),
					) || "0 sec"}\``,
					// ❯ **Total:** \`${amount.msgs + amount.vc + (vc ? vc.messages : 0)}\``,
				)
				.setTimestamp()
				.setThumbnail(m.author.avatarURL)
				.setFooter(`${m.author.tag}`);

			m.channel.send({ embed });
		});

		this.client.command(
			"quota.records",
			20,
			"[member:member]",
			async (c, m, a: [GuildMember]) => {
				const SFE = this.client.guilds.get("360462032811851777");
				if (!SFE) {
					return;
				}

				// If argument is provided
				if (a[0]) {
					if (
						!a[0].roles.find((r) => r.name === "• Moderation Staff")
					) {
						return err(m, "That user is not a staff member");
					}
					const amount = await this.getEntry(a[0].id);
					if (!amount) {
						return err(m, "Could not find user");
					}
					const vc = this.config.activeVoiceSessions.get(m.author.id);
					return m.author
						.send({
							embed: new Embed(this.client)
								.setTitle(`Quota Progress for ${a[0].user.tag}`)
								.setDescription(
									`${
										this.config.activeVoiceSessions.get(
											m.author.id,
										)
											? "User is currently in a VC. Below values are approximations.\n\n"
											: ""
									}❯ **Messages:** \`${amount.msgs}\`

❯ **Voice Channel Quota**
VC Messages: \`${vc ? amount.vc + vc.messages : amount.vc}\`
Equivalent Time: \`${juration.stringify(
										(amount.vc / 50) * 60 ** 2 +
											(vc
												? Math.floor(vc.length / 1000)
												: 0),
									) || "0 sec"}\`

❯ **Total:** \`${amount.msgs + amount.vc + (vc ? vc.messages : 0)}\``,
								)
								.setTimestamp()
								.setThumbnail(a[0].user.avatarURL)
								.setFooter(`${a[0].user.tag}`),
						})
						.then(
							() => {
								check(
									m,
									"Sent records to DMs to avoid suspicious eyes :eyes:",
								);
							},
							() => {
								err(
									m,
									"Could not DM you the VC quota. Have u blocked me? :(",
								);
							},
						);
				}

				// If no argument is provided:
				const date = new QuotaWeek();
				const users = await QuotaUserModel.find({
					week: `${date.getFullYear()}-${date.getWeek()}`,
				});

				users.sort((x, y) => y.msgs + y.vc - (x.msgs + x.vc));

				return m.author
					.send(
						{
							embed: new Embed(this.client)
								.setTitle("SFE Staff Quota")
								.setDescription(
									`Here are the current quota standings. All values represent either exact, or equivalent messages.\n\n${users
										.map((user, i) =>
											user
												? `${i}. <@${user.uid}> (\`${
														user.uid
												  }\`) - **${user.msgs +
														user.vc}** (msgs: \`${
														user.msgs
												  }\`, vc: \`${user.vc}\`)`
												: "No records for this week.",
										)
										.join(
											"\n",
										)}\n\nRun \`s!quota json\` to generate a JSON file for usage in Excel/Google Sheets. :3`,
								)
								.addField(
									"Total Messages",
									users
										.map((v) => v.msgs + v.vc)
										.reduce((p, v) => p + v, 0),
									true,
								)
								.addField(
									"Chat Messages",
									users
										.map((v) => v.msgs)
										.reduce((p, v) => p + v, 0),
									true,
								)
								.addField(
									"VC Messages",
									users
										.map((v) => v.vc)
										.reduce((p, v) => p + v, 0),
									true,
								)
								.addField(
									"Most Active Staff Member",
									SFE.members.get(
										users[0].uid,
									) as GuildMember,
								),
						},
						{ split: true },
					)
					.then(
						() => {
							check(
								m,
								"Sent records to DMs to avoid suspicious eyes :eyes:",
							);
						},
						() => {
							err(
								m,
								"Could not DM you the VC quota. Have u blocked me? :(",
							);
						},
					);
			},
		);

		// Converts quota data into JSON.
		this.client.command(
			"quota.json",
			20,
			"[week:string]",
			async (c, m, a) => {
				const SFE = this.client.guilds.get("360462032811851777");
				if (!SFE) {
					return;
				}

				const date =
					a[0] ||
					`${new QuotaWeek().getFullYear()}-${new QuotaWeek().getWeek()}`;

				const users = await QuotaUserModel.find({
					week: date,
				});

				if (users.length < 1) {
					if (a[0]) {
						return err(
							m,
							`Could not find any data for week \`${
								a[0]
							}\`. Is the week valid?`,
						);
					} else {
						return err(m, "No data available.");
					}
				}

				const entries = users.map((v) => {
					return {
						id: v.uid,
						msgs: v.msgs,
						total: v.msgs + v.vc,
						vc: v.vc,
					};
				});
				writeFileSync(
					`${__dirname}/${date}.json`,
					JSON.stringify(entries),
				);

				await m.author
					.send(`JSON Data for ${date}`, {
						file: `${__dirname}/${date}.json`,
					})
					.then(
						() => {
							check(
								m,
								"Sent records to DMs to avoid suspicious eyes :eyes:",
							);
						},
						() => {
							err(
								m,
								"Could not DM you the VC quota. Have u blocked me? :(",
							);
						},
					);

				unlinkSync(`${__dirname}/${date}.json`);
			},
		);

		process.on("SIGINT", async () => {
			this.client.logger.warn("SIGINT detected! Exiting gracefully...");
			const iterator = this.config.activeVoiceSessions.map((v) =>
				v.emit("end", "BOT_STOPPING"),
			);
			await Promise.all(iterator);
			setTimeout(() => {
				this.logger.info("Task complete. Have a nice day!");
				process.exit(0);
			}, 2e3);
		});

		// Quota Tracking
		this.on("message", (m: Message) => {
			const SFE = this.client.guilds.get("360462032811851777");
			if (!SFE || !m.guild || m.guild.id !== SFE.id || !m.member) {
				return;
			}

			if (
				m.member.roles.find(
					(r: Role) => r.name === "• Moderation Staff",
				) &&
				["360462032811851778"].indexOf(m.channel.id) > -1
			) {
				this.recordEntry(m.author.id, 1, "msg");
			}
		});

		this.logger.info("[QUOTA] Ready.");
	}

	public checkForStaffInVC(o?: GuildMember, n?: GuildMember) {
		const SFE = this.client.guilds.get("360462032811851777");
		if (!SFE) {
			return;
		}

		// If triggered by voiceStateUpdate
		if (o && n) {
			if (!n.roles.find((r) => r.name === "• Moderation Staff")) {
				return;
			}
			// If staff has connected, create session
			if (!o.voiceChannel && n.voiceChannel) {
				return this.createSession(n);
			}
		}

		this.logger.debug("[VC] Checking for staff in VC...");

		SFE.members
			.filter((v) =>
				v.roles.find((r) => r.name === "• Moderation Staff")
					? true
					: false,
			)
			.map((m) => {
				if (
					m.voiceChannel &&
					!this.config.activeVoiceSessions.get(m.id)
				) {
					return this.createSession(m);
				}
			});
	}

	// Create session for member
	public createSession(m: GuildMember) {
		// Just in case something broke - don't create if no VC
		if (!m.voiceChannel) {
			return;
		}
		if (m.voiceChannel.members.filter((v) => !v.mute).size < 3) {
			return this.logger.info(
				`[VC] CHANNEL for ${m.user.tag} (${
					m.id
				}) does not have enough members.`,
			);
		}

		const session = new VCQuotaSession(this.client, m).on("end", (r) => {
			this.endSession(m, r);
		});

		this.config.activeVoiceSessions.set(m.id, session);
	}

	// End voice session - handles data adding etc.
	public async endSession(m: GuildMember, reason?: string) {
		// Get session to end in question
		let vc:
			| VCQuotaSession
			| undefined = this.config.activeVoiceSessions.get(m.id);

		if (!vc) {
			return this.logger.warn(
				`[VC] UNTRACKED SESSION ${m.user.tag} ${m.id}`,
			);
		}

		// Delete entry
		this.config.activeVoiceSessions.delete(m.id);

		// DM staff if enabled
		if (vc.messages > 0) {
			if (this.config.dmExceptions.indexOf(m.id) > -1) {
				this.logger.info(`[VC] Notifying ${m.user.tag} of their quota progress...`);
				await m.send(
					`**Staff Voice Sessions:** Your latest session has ended. It lasted ${juration.stringify(
						Math.round(vc.length / 1000),
					)}, and is the equivalent of ${Math.floor(
						vc.messages,
					)} messages. ${reason ? `\`${reason}\`` : ""}

:information_source: You can disable these notifications by performing \`s!quota toggle\` in #staff-chat.`,
				);
				// Warning message
			}
			this.recordEntry(m.id, vc.messages, "vc");
		}

		this.logger.info(`[VC] END Session for ${m.user.tag} (${m.id}) - ${Date.now() - vc.createdAt}`);
		vc = undefined;
	}

	// Function for recording quota data to database.
	private async recordEntry(id: string, count: number, type: "vc" | "msg") {
		const date = new QuotaWeek();
		await QuotaUserModel.findOneAndUpdate(
			{
				uid: id,
				week: `${date.getFullYear()}-${date.getWeek()}`,
			},
			{
				$inc: {
					msgs: type === "msg" ? count : 0,
					vc: type === "vc" ? count : 0,
				},
				uid: id,
				week: `${date.getFullYear()}-${date.getWeek()}`,
			},
			{
				upsert: true,
			},
		);
	}

	private async getEntry(id: string) {
		const date = new QuotaWeek();
		const user = await QuotaUserModel.findOne({
			uid: id,
			week: `${date.getFullYear()}-${date.getWeek()}`,
		});
		if (user) {
			return new QuotaUser(user);
		} else {
			return false;
		}
	}
}
