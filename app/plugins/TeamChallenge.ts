import { Message, TextChannel } from "discord.js";
import { Embed, Plugin, PluginWithConfig, SparklClient } from "sparkldjs/src";

import { TeamChallenge } from "../structures/challenge/TeamChallenge";
import { check, err } from "../util/DefaultEmbeds";

export class TeamChallengePlugin extends PluginWithConfig<{
	currentChallenge?: TeamChallenge;
}> {
	constructor(c: SparklClient) {
		super(c);
		this.pluginName = "TeamChallengePlugin";
		this.config = {};
	}

	public async init() {
		this.client.command(
			"tc.create",
			30,
			"<channel:string> <message:string> [gracePeriod:duration]",
			async (c, m, a: [string, string, number]) => {
				if (this.config.currentChallenge) {
					return err(
						m,
						"There is already a team challenge in progress!",
					);
				}

				const CHANNEL = m.guild.channels.get(a[0]);
				if (!CHANNEL || !(CHANNEL instanceof TextChannel)) {
					return err(m, "Could not find channel.");
				}
				const MESSAGE = (await CHANNEL.fetchMessage(a[1])) as Message;
				if (!MESSAGE) {
					return err(m, "Could not find message.");
				}

				this.config = {
					currentChallenge: new TeamChallenge(this.client, MESSAGE, {
						preparationPhaseLength: a[2] || 2.16e7,
					}),
				};
			},
		);

		this.client.command(
			"tc.config",
			0,
			"[property:string] [newValue:string]",
			(c, m, a: [string, string]) => {
				if (!this.config.currentChallenge) {
					return err(
						m,
						"There is not currently a challenge in progress!",
					);
				}
				if (a[0]) {
					switch (a[0]) {
						case "logChannel": {
							if (a[1]) {
								const CHANNEL = m.guild.channels.get(a[1]);
								if (
									!CHANNEL ||
									!(CHANNEL instanceof TextChannel)
								) {
									err(m, "Could not find channel.");
								} else {
									this.config.currentChallenge.options.logChannel =
										CHANNEL.id;
									check(m, "Updated `logChannel` property.");
								}
							} else {
								m.reply(
									`**Log Channel:** ${
										this.config.currentChallenge.options
											.logChannel
											? `<#${
													this.config.currentChallenge
														.options.logChannel
											  }>`
											: "None set"
									}`,
								);
							}
						}
					}
				} else {
					return m.channel.send({
						embed: new Embed(this.client)
							.setTitle("Config for Team Challenge")
							.setDescription(
								"Here is the current configuration for the ongoing team challenge.",
							)
							.addField(
								"Logging Channel",
								this.config.currentChallenge.options
									.logChannel || "None set",
							),
					});
				}
			},
		);
	}
}
