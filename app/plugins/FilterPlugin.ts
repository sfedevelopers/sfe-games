import { Message, TextChannel } from "discord.js";
import * as moment from "moment";
// @ts-ignore
import * as SightEngine from "sightengine";
import { Embed, PluginWithConfig, SparklClient } from "sparkldjs/src";
import { isUndefined } from "util";

import { check, err } from "../util/DefaultEmbeds";

const imageChecker = SightEngine("650896890", "3za6ZVMAqxedwFRe7kwU");

const CHARACTER_ALTERNATIVES = [
	{
		char: "a",
		equivalent: "[aáạàảãăắặằẳẵâấậầẩẫåæ]",
	},
	{
		char: "e",
		equivalent: "[eéẹèẻẽêếệềểễ3æ]",
	},
	{
		char: "i",
		equivalent: "[iíịìỉĩ1!]",
	},
	{
		char: "o",
		equivalent: "[oóọòỏõôốộồổỗơớợờởỡøx]",
	},
	{
		char: "u",
		equivalent: "[uúụùủũưứựừửữvy]",
	},
];

const GLOBAL_FILTER_TOKENS = [
	"\\bas{2,}\\b",
	"\\banal\\b",
	"anus",
	"b(i|[*-.])tch",
	"bitch",
	"blowjob",
	"bo{2,}b",
	"boner",
	"c(o|[*-.])ck",
	"c(u|[*-.])(n|[*-.])t",
	"clit(oris)?",
	"co{2,}n",
	"cuck",
	"\\bcum\\b",
	"dick",
	"dildo",
	"douche(bag)?",
	"dune",
	"f(a|[*-.])ggot",
	"fag{2,}(ot)?",
	"fap",
	"feuj",
	"hentai",
	"\\bhoe(s)?\\b",
	"indog",
	"jewboy",
	"jiz{2,}",
	"kontol",
	"kys",
	"loli\\b",
	"masturbation",
	"milf",
	"n(i|[*-.])[gb*]{2,}(er|a|ar)",
	"orgy",
	"oven dodger",
	"penis",
	"porn",
	"prostitute",
	"pussy",
	"\\br(a|[*-.])p{1,1}e",
	"r(e|[*-.])tard",
	"\\bsemen",
	"slut",
	"sperm",
	"vagina(l)?",
	"whore",
	"卐",
	// 	"(gay)",
];

const CRITICAL_FILTER_TOKENS = ["n(i|[*-.])[gb*]{2,}(er|a|ar)"];
const ALL_MEMBERS_FILTER_TOKENS = ["f(u|[*-.])(c|[*-.]|x)k"];

const createFilter = (tokens: string[]) => {
	return new RegExp(
		tokens
			.map((v) => {
				let withPlus = v.replace(
					/(?<!\\)[a-z](?![^\[]*\])(?!{)/gi,
					"$&+",
				);

				CHARACTER_ALTERNATIVES.map((ch) => {
					withPlus = withPlus.replace(ch.char, ch.equivalent);
				});

				return `(${withPlus})`;
			})
			.join("|"),
		"gi",
	);
};

const filterStr = (msg: string, filter: RegExp) => {
	const match = msg.match(filter);
	if (match) {
		return match.filter(
			(v, i) => !isUndefined(v) && v.length > 1 && match.indexOf(v) === i,
		);
	} else {
		return null;
	}
};

const GLOBAL_FILTER = createFilter(GLOBAL_FILTER_TOKENS);
const CRITICAL_FILTER = createFilter(CRITICAL_FILTER_TOKENS);
const ALL_MEMBERS_FILTER = createFilter(ALL_MEMBERS_FILTER_TOKENS);

export class FilterPlugin extends PluginWithConfig<{
	image: number;
	swear: boolean;
}> {
	public constructor(client: SparklClient) {
		super(client);

		this.pluginName = "FilterPlugin";
		this.config = {
			image: 0,
			swear: false,
		};
	}
	public async init() {
		this.on("message", (m: Message) => {
			if (m.author.bot) {
				return;
			}

			if (!m.guild) {
				return;
			}

			if (m.member.roles.find((r) => r.name === "• Moderation Staff")) {
				return;
			}

			if (this.config.swear) {
				const match = m.cleanContent.trim().match(GLOBAL_FILTER);
				const criticalMatch = m.cleanContent
					.trim()
					.match(CRITICAL_FILTER);

				const allMembersMatch =
					m.channel.id === "360462032811851778"
						? filterStr(m.cleanContent.trim(), ALL_MEMBERS_FILTER)
						: null;

				const cleanMatch = allMembersMatch ? allMembersMatch : match;

				if (cleanMatch) {
					m.delete().then(
						() => {
							const LOG_CHANNEL = m.guild.channels.get(
								"410176566773940224",
							) as TextChannel;
							if (LOG_CHANNEL) {
								LOG_CHANNEL.send(
									`\`[${moment().format(
										"HH:mm:ss",
									)}]\`:no_entry_sign: censored message by ${
										m.author.tag
									} (\`${m.author.id}\`) in #${
										(m.channel as TextChannel).name
									} found${
										criticalMatch ? " critical" : ""
									} blacklisted words \`${cleanMatch.join(
										", ",
									)}\`: \`\`\`${m.cleanContent}\`\`\``,
								);
							}
						},
						() => {
							return;
						},
					);
					if (criticalMatch) {
						const LOG_CHANNEL = m.guild.channels.get(
							"417710433533689866",
						) as TextChannel;
						if (LOG_CHANNEL) {
							LOG_CHANNEL.send(
								`**Critical Filter Violation:** ${
									m.author.tag
								} (\`${
									m.author.id
								}\`) - check <#410176566773940224>`,
							);
						}
					}
				}
			}
			if (this.config.image > 0 && m.attachments) {
				m.attachments.map(async (v, k) => {
					try {
						const result = await imageChecker
							.check(["nudity", "wad", "offensive"])
							.set_url(v.url);

						if (
							result.nudity &&
							result.nudity.raw >=
								Math.max(
									result.nudity.partial,
									result.nudity.safe,
								)
						) {
							const LOG_CHANNEL = m.guild.channels.get(
								"417710433533689866",
							) as TextChannel;
							if (LOG_CHANNEL) {
								LOG_CHANNEL.send({
									embed: new Embed(this.client)
										.setTitle("Image Filter Alert")
										.setDescription(
											`Potential NSFW Image Found`,
										)
										.addField(
											"Member",
											`${m.author.tag}, <@${
												m.author.id
											}> (\`${m.author.id}\`)`,
											true,
										)
										.addField(
											"Channel",
											`${
												(m.channel as TextChannel).name
											}, <#${m.channel.id}> (\`${
												m.channel.id
											}\`)`,
										)
										.setImage(v.url),
								});
							}
							if (this.config.image === 2 && m.deletable) {
								m.delete().catch(() => {
									return;
								});
							}
						} else if (
							result.offensive &&
							result.offensive.prob &&
							result.offensive.prob > 0.75
						) {
							const LOG_CHANNEL = m.guild.channels.get(
								"417710433533689866",
							) as TextChannel;
							if (LOG_CHANNEL) {
								LOG_CHANNEL.send({
									embed: new Embed(this.client)
										.setTitle("Image Filter Alert")
										.setDescription(
											`Potential Offensive Image Found`,
										)
										.addField(
											"Member",
											`${m.author.tag}, <@${
												m.author.id
											}> (\`${m.author.id}\`)`,
											true,
										)
										.addField(
											"Channel",
											`${
												(m.channel as TextChannel).name
											}, <#${m.channel.id}> (\`${
												m.channel.id
											}\`)`,
										)
										.setImage(v.url),
								});
								if (this.config.image === 2 && m.deletable) {
									m.delete().catch(() => {
										return;
									});
								}
							}
						} else if (result.weapon && result.weapon > 0.65) {
							const LOG_CHANNEL = m.guild.channels.get(
								"417710433533689866",
							) as TextChannel;
							if (LOG_CHANNEL) {
								LOG_CHANNEL.send({
									embed: new Embed(this.client)
										.setTitle("Image Filter Alert")
										.setDescription(
											`Potential Inappropriate	 Image Found`,
										)
										.addField(
											"Member",
											`${m.author.tag}, <@${
												m.author.id
											}> (\`${m.author.id}\`)`,
											true,
										)
										.addField(
											"Channel",
											`#${
												(m.channel as TextChannel).name
											}, <#${m.channel.id}> (\`${
												m.channel.id
											}\`)`,
											true,
										)
										.setImage(v.url),
								});
								if (this.config.image === 2 && m.deletable) {
									m.delete().catch(() => {
										return;
									});
								}
							}
						}
					} catch (err) {
						this.logger.error(err);
					}
				});
			}
		});
		this.client.command(
			"filter",
			20,
			"<toToggle:string> [level:number]",
			(c, m, a: [string, number]) => {
				switch (a[0]) {
					case "swear": {
						this.updateConfig({ swear: !this.config.swear });
						return check(
							m,
							`${
								this.config.swear ? "Enabled" : "Disabled"
							} swear filter.`,
						);
					}
					case "image": {
						if (!a[1]) {
							return err(
								m,
								"Please select a level: 0 - Disabled, 1 - Warning, 2 - Deletion",
							);
						}
						switch (a[1]) {
							case 0: {
								this.updateConfig({
									image: 0,
								});
								return check(m, `Disabled image filter.`);
							}
							case 1: {
								this.updateConfig({
									image: 1,
								});
								return check(
									m,
									`Enabled image filter in **WARNING** mode.`,
								);
							}
							case 2: {
								this.updateConfig({
									image: 2,
								});
								return check(
									m,
									`Enabled image filter in **DELETION** mode.`,
								);
							}
							default: {
								return err(
									m,
									"Please select a valid level: 0 - Disabled, 1 - Warning, 2 - Deletion",
								);
							}
						}
					}
					default: {
						return err(
							m,
							"Please select a valid filter option: `image`, `swear`.",
						);
					}
				}
			},
		);
		this.logger.info("[FILTER] Done.");
	}
}
