import { Message } from "discord.js";
import { SparklClient } from "sparkldjs/src";

import { BasePlugin } from "./plugins/BasePlugin";
import { FilterPlugin } from "./plugins/FilterPlugin";
import { FurPlugin } from "./plugins/FurPlugin";
import { QuotaPlugin } from "./plugins/QuotaPlugin";
import { TeamChallengePlugin } from "./plugins/TeamChallenge";
import { UtilPlugin } from "./plugins/UtilPlugin";
import { TOKEN } from "./token";
import { err } from "./util/DefaultEmbeds";

const bot = new SparklClient({
	loggerDebugLevel: 2,
	name: "SFE Games",
	syntaxErrorHandler: (m: Message, sx) => {
		switch (sx.type) {
			case "TOO_MANY_ARGS":
			case "NOT_ENOUGH_ARGS": {
				return err(
					m,
					`Malformed syntax! I was expecting \`${
						sx.expectedArgument
							? sx.expectedArgument.string
							: "none"
					}\`.`,
				);
			}
			case "PARSE_FAILED": {
				return err(
					m,
					`Oops! Could not parse \`${
						sx.recievedArgument ? sx.recievedArgument.value : "none"
					}\` to type \`${
						sx.expectedArgument
							? sx.expectedArgument.string
							: "none"
					}\`.`,
				);
			}
		}
	},
	token: TOKEN,
}).addPlugin(
	BasePlugin,
	FurPlugin,
	QuotaPlugin,
	UtilPlugin,
	FilterPlugin,
	TeamChallengePlugin,
	// VCPlugin,
);
bot.login();
