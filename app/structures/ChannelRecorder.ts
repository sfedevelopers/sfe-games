import { User, VoiceConnection, VoiceReceiver } from "discord.js";
import { EventEmitter } from "events";
import { SparklClient } from "sparkldjs/src";

export class ChannelRecorder extends EventEmitter {
	public client: SparklClient;
	public connection: VoiceConnection;

	private receiver: VoiceReceiver;

	constructor(client: SparklClient, connection: VoiceConnection) {
		super();

		this.client = client;
		this.connection = connection;

		this.receiver = this.connection.createReceiver();
		this.receiver.on("opus", (user: User) => {
			this.receiver
				.createOpusStream(user)
				.on("data", () => this.client.logger.debug("data"));
		});

		this.connection.on("speaking", (u, s) => {
			if (s) {
				this.receiver
					.createOpusStream(u)
					.on("data", () => this.client.logger.debug("data"));
			}
		});
	}
}
