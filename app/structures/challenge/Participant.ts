import { GuildMember } from "discord.js";
import { EventEmitter } from "events";
import { SparklClient } from "sparkldjs/src";

export class Participant extends EventEmitter {
	public client: SparklClient;
	public active: boolean;

	public member: GuildMember;

	constructor(client: SparklClient, member: GuildMember) {
		super();
		this.client = client;
		this.active = false;

		this.member = member;

		this.client.on("guildMemberRemove", (m: GuildMember) => {
			if (this.member.id === m.id) {
				this.emit("end");
			}
		});
	}
}
