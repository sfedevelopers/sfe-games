import { Collection, GuildMember, Message, MessageReaction, TextChannel, User } from "discord.js";
import { EventEmitter } from "events";
// @ts-ignore
import * as juration from "juration";
import * as moment from "moment";
import { SparklClient } from "sparkldjs/src";

import { check } from "../../util/DefaultEmbeds";
import { Participant } from "./Participant";

export interface TeamChallengeOptions {
	preparationPhaseLength: number;
	logChannel?: string;
}

/**
 * Class to represent a team challenge.
 * @param {SparklClient} client - client that insitated the object
 * @param {Message} message - the message to measure reactions from
 */
export class TeamChallenge extends EventEmitter {
	public client: SparklClient;

	public createdAt: number;
	public state: "PREPARE" | "ACTIVE" | "ENDED";
	public active: boolean;

	public message: Message;

	public options: TeamChallengeOptions;

	public participants: Collection<string, Participant>;

	constructor(
		client: SparklClient,
		message: Message,
		options?: TeamChallengeOptions,
	) {
		super();
		this.client = client;

		this.state = "PREPARE";
		this.active = false;

		this.message = message;

		this.options = Object.assign(
			{ preparationPhaseLength: 2.16e7 },
			options,
		);
		this.participants = new Collection();
		this.createdAt = Date.now();

		this.createCollector();

		this.on("participantAdd", (m: GuildMember) => {
			this.log(`**Participant Added:** ${m.user.tag} (\`${m.id}\`)`);
		});

		this.on("participantRemove", (m: GuildMember) => {
			this.log(`**Participant Removed:** ${m.user.tag} (\`${m.id}\`)`);
		});

		check(
			this.message,
			`Successfully created a new team challenge! It will start in ${juration.stringify(
				this.options.preparationPhaseLength / 1000,
			)}.`,
		);
	}

	// JSONification
	public toJSON() {
		return JSON.stringify({
			active: this.active,
			createdAt: this.createdAt,
			message: [
				this.message.guild,
				this.message.channel.id,
				this.message.id,
			],
			state: this.state,
		});
	}
	private async createCollector() {
		await this.message.react("✅");
		this.message
			.createReactionCollector(
				(v: MessageReaction, user: User) =>
					user.id !== this.client.user.id &&
					!this.participants.has(user.id),
				{ time: this.options.preparationPhaseLength },
			)
			.on("collect", async (r: MessageReaction) => {
				const iterator = r.users.map(async (u: User) => {
					if (
						this.participants.has(u.id) ||
						u.id === this.client.user.id
					) {
						return;
					}

					let MEMBER = this.message.guild.members.get(u.id);
					if (!MEMBER) {
						MEMBER = await this.message.guild.fetchMember(
							u.id,
							true,
						);
					}
					if (MEMBER) {
						this.participants.set(
							u.id,
							new Participant(this.client, MEMBER),
						);
						this.emit("participantAdd", MEMBER);
						this.client.logger.log("[TC] New participant", u.id);
					}
				});
				await Promise.all(iterator);

				this.checkIfReactionUsersAreParticipants();
			});
	}

	private checkIfReactionUsersAreParticipants() {
		const R = this.message.reactions.find((r) => r.emoji.name === "✅");
		if (R) {
			this.participants
				.filter((v) => !R.users.has(v.member.id))
				.map((p) => {
					this.participants.delete(p.member.id);
					this.emit("participantRemove", p.member);
				});
		}
	}

	private log(m: string) {
		if (this.options.logChannel) {
			const CHANNEL = this.message.guild.channels.get(
				this.options.logChannel,
			) as TextChannel;
			if (CHANNEL) {
				CHANNEL.send(`\`[${moment().format("HH:mm:ss")}]\` [TC] ${m}`);
			}
		}
	}
}
