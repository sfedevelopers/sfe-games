import { GuildMember, GuildMemberResolvable } from "discord.js";
import { EventEmitter } from "events";
import { SparklClient } from "sparkldjs/src";

export declare interface VCQuotaSession extends EventEmitter {
	client: SparklClient;
	member: GuildMember;
	createdAt: number;

	on: (event: "end", handler: (reason?: "NOT_ENOUGH_USERS") => any) => any;
}

const SPECIAL_CONDITIONS = [
	"195965128343027712",
	"332209233577771008",
	"373000529472782336",
];

export class VCQuotaSession extends EventEmitter {
	public client: SparklClient;
	public member: GuildMember;

	public createdAt: number;
	public currentChannel: string;

	constructor(client: SparklClient, member: GuildMemberResolvable) {
		super();
		this.client = client;

		if (member instanceof GuildMember) {
			if (member.guild.id !== "360462032811851777") {
				throw Error("Session created outside of SFE.");
			}
			this.member = member;
		} else {
			const SFE = client.guilds.get("360462032811851777");
			if (!SFE) {
				throw Error("Could not find SFE.");
			}
			const m = SFE.members.get(member.id);
			if (!m) {
				throw Error("Could not find member " + member.id);
			}
			this.member = m;
		}
		this.createdAt = Date.now();
		this.currentChannel = this.member.voiceChannelID;

		this.client.on("voiceStateUpdate", (o: GuildMember, n: GuildMember) => {
			if (
				o.id !== this.member.id &&
				o.voiceChannel &&
				o.voiceChannel.id === this.currentChannel &&
				!n.voiceChannel
			) {
				if (
					this.member.voiceChannel &&
					this.member.voiceChannel.members.filter((v) => !v.mute)
						.size < 3
				) {
					this.emit("end", "NOT_ENOUGH_USERS");
				}
			}

			if (o.id !== this.member.id) {
				return;
			}
			this.emit("update", o, n);
			if (
				o.voiceChannel &&
				n.voiceChannel &&
				o.voiceChannel.id !== n.voiceChannel.id
			) {
				this.client.logger.debug(
					`[VC] CHANGE for ${this.member.user.tag} (${
						this.member.id
					}) - ${o.voiceChannel.id} => ${n.voiceChannel.id}`,
				);
				this.currentChannel = n.voiceChannel.id;
			}

			if (!n.voiceChannel || !this.member.voiceChannel) {
				return this.emit("end");
			}

			if (n.voiceChannel.members.filter((v) => !v.mute).size < 3) {
				this.emit("end", "NOT_ENOUGH_USERS");
			}
		});
		this.client.logger.debug(
			`[VC] BEGIN Session for ${this.member.user.tag} (${
				this.member.id
			})`,
		);
	}

	get length() {
		return Date.now() - this.createdAt;
	}

	get messages() {
		if (SPECIAL_CONDITIONS.indexOf(this.member.id) !== -1) {
			return Math.floor(this.length / 144000) > 300
				? 300
				: Math.floor(this.length / 144000);
		}
		return Math.floor(this.length / 72000) > 300
			? 300
			: Math.floor(this.length / 72000);
	}
}
