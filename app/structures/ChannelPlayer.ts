import { StreamDispatcher, VoiceConnection } from "discord.js";
import { EventEmitter } from "events";
import { SparklClient } from "sparkldjs/src";
import * as ytdl from "ytdl-core";

export class ChannelPlayer extends EventEmitter {
	get currentSong() {
		return this.queue[0];
	}
	get nextSong() {
		return this.queue[1];
	}
	public client: SparklClient;
	public connection: VoiceConnection;

	public queue: ytdl.videoInfo[];

	private dispatcher?: StreamDispatcher;

	constructor(client: SparklClient, connection: VoiceConnection) {
		super();

		this.client = client;
		this.connection = connection;

		this.queue = [];
	}

	public async addToQueue(url: string): Promise<ytdl.videoInfo> {
		if (!ytdl.validateURL(url)) {
			throw Error("Invalid URL");
		}
		const info = await new Promise<ytdl.videoInfo>((resolve, reject) => {
			ytdl.getInfo(url, (err, i) => {
				if (err) {
					reject(err);
				}
				this.emit("queueAdd");
				resolve(i);
			});
		});
		this.queue.push(info);
		return info;
	}

	public skip() {
		if (this.dispatcher && !this.dispatcher.destroyed) {
			this.emit("skip", this.currentSong);
			this.dispatcher.end();
		}
	}

	public setVolume(volume: number) {
		if (this.dispatcher && !this.dispatcher.destroyed) {
			this.dispatcher.setVolume(volume);
			this.emit("update", { type: "VOLUME" });
		}
	}

	public pause() {
		if (this.dispatcher && !this.dispatcher.destroyed) {
			this.dispatcher.pause();
			this.emit("update", { type: "PAUSE" });
		}
	}

	public resume() {
		if (this.dispatcher && !this.dispatcher.destroyed) {
			this.dispatcher.resume();
			this.emit("update", { type: "RESUME" });
		}
	}

	private async playURL(url: string | ytdl.videoInfo) {
		let videoInfo: ytdl.videoInfo;
		if (typeof url === "string") {
			if (!ytdl.validateURL(url)) {
				this.emit("error", "INVALID_URL");
			}
			videoInfo = await new Promise((res, rej) => {
				ytdl.getInfo(url, (err, info) => {
					if (err) {
						return this.emit("error", "CANNOT_GET");
					}
					res(info);
				});
			});
		} else {
			videoInfo = url;
		}

		this.dispatcher = this.connection
			.playStream(
				ytdl.downloadFromInfo(videoInfo, { filter: "audioonly" }),
			)
			.on("end", () => this.playFromQueue());
		this.emit("play", videoInfo);
	}

	private playFromQueue() {
		this.queue.shift();
		const nextSong = this.queue[0];

		if (nextSong) {
			this.playURL(nextSong);
		}
	}
}
