export interface ClientUserData {
	expiresAt: number;
	uid: string;
	token: string;
}

export declare interface ClientUser {
	expiresAt: number;
	uid: string;
	token: string;
}

export class ClientUser {
	public expiresAt: number;
	public uid: string;
	public token: string;

	constructor(data: ClientUserData) {
		this.expiresAt = data.expiresAt;
		this.uid = data.uid;
		this.token = data.token;
	}

	public toModel() {
		return {
			expiresAt: this.expiresAt,
			token: this.token,
			uid: this.uid,
		};
	}

	get expired() {
		return Date.now() < this.expiresAt;
	}
}
