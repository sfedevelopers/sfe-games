import { Permissions } from "discord.js";

export interface DiscordAPIPartialGuild {
	id: string;
	name: string;
	icon: string;
	owner: boolean;
	permissions: number;
}

export declare interface PartialGuild {
	id: string;
	name: string;
	icon: string;
	owner: boolean;
	permissions: number;
}
export class PartialGuild {
	public id: string;
	public name: string;
	public icon: string;
	public owner: boolean;
	public permissions: number;

	constructor(data: DiscordAPIPartialGuild) {
		this.id = data.id;
		this.name = data.name;
		this.icon = data.icon;
		this.owner = data.owner;
		this.permissions = data.permissions;
	}

	public get hasPermission() {
		return new Permissions(this.permissions).serialize(true).MANAGE_ROLES;
	}

	public toJSON() {
		return {
			hasPermission: this.hasPermission,
			icon: this.icon,
			id: this.id,
			name: this.name,
			owner: this.owner,
			permissions: this.permissions,
		};
	}
}
