export interface DiscordAPIUserData {
	avatar: string;
	discriminator: string;
	id: string;
	username: string;
	bot?: boolean;
	email?: string;
	flags?: number;
	locale?: string;
	mfa_enabled?: boolean;
	premium_type?: number;
	verified?: boolean;
}

export declare interface User {
	avatar: string;
	discriminator: string;
	uid: string;
	username: string;
}

export class User {
	public avatar: string;
	public discriminator: string;
	public uid: string;
	public username: string;

	constructor(data: DiscordAPIUserData) {
		this.uid = data.id;
		this.avatar = data.avatar;
		this.username = data.username;
		this.discriminator = data.discriminator;
	}

	get tag() {
		return `${this.username}#${this.discriminator}`;
	}

	public toModel() {
		return {
			avatar: this.avatar,
			tag: this.tag,
			uid: this.uid,
		};
	}
}
