export declare interface QuotaUser {
	msgs: number;
	uid: string;
	vc: number;
	week: string;
}

export class QuotaUser {
	public msgs: number;
	public uid: string;
	public vc: number;
	public week: string;

	constructor(data: {
		uid: string;
		msgs?: number;
		vc?: number;
		week: string;
	}) {
		this.msgs = data.msgs || 0;
		this.uid = data.uid;
		this.vc = data.vc || 0;
		this.week = data.week;
	}

	public toModel() {
		return {
			msgs: this.msgs,
			uid: this.uid,
			vc: this.vc,
			week: this.week,
		};
	}
}
