import { Document, Model, model, Schema } from "mongoose";

import { QuotaUser } from "../structures/QuotaUser";

export interface QuotaUserDocument extends QuotaUser, Document {}

export const QuotaUserSchema: Schema = new Schema({
	msgs: Number,
	uid: String,
	vc: Number,
	week: String,
});

export const QuotaUserModel: Model<QuotaUserDocument> = model<
	QuotaUserDocument
>("QuotaUser", QuotaUserSchema);
