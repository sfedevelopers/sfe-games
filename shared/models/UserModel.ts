import { Document, Model, model, Schema } from "mongoose";

import { User } from "../structures/User";

export interface UserDocument extends User, Document {}

export const UserSchema: Schema = new Schema({
	avatar: String,
	tag: String,
	uid: String,
});

export const UserModel: Model<UserDocument> = model<UserDocument>(
	"User",
	UserSchema,
);
