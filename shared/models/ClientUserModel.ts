import { Document, Model, model, Schema } from "mongoose";
import { ClientUser } from "../structures/ClientUser";

export interface ClientUserDocument extends ClientUser, Document {}

export const ClientUserSchema: Schema = new Schema({
	expiresAt: Number,
	token: String,
	uid: String,
});

export const ClientUserModel: Model<ClientUserDocument> = model<
	ClientUserDocument
>("ClientUser", ClientUserSchema);
