import "colors";

import * as express from "express";
import * as http from "http";
import { default as morgan } from "morgan";
import * as winston from "winston";

interface ServerOptions {
	port?: number;
	debug?: boolean;
	prefix?: string;
}

export class SocketServer {
	public options: ServerOptions;
	public logger: winston.Logger;
	public app: express.Application;
	private socket: http.Server;

	constructor(opts?: ServerOptions) {
		this.options = opts || {
			debug: false,
			port: 8080,
			prefix: "",
		};
		this.logger = winston.createLogger({
			format: winston.format.combine(
				winston.format.colorize(),
				winston.format.timestamp(),
				winston.format.simple(),
				winston.format.printf((info) => {
					const { timestamp, level, message } = info;

					const ts = timestamp.slice(0, 19).split("T")[1];
					return `[${ts.gray}]${
						this.options.prefix ? `[${this.options.prefix}]` : ""
					}[${level}] ${message}`;
				}),
			),
			transports: new winston.transports.Console({
				level: this.options.debug ? "debug" : "info",
			}),
		});
		this.app = express.default();
		this.socket = new http.Server(this.app);

		this.app.use(
			morgan("dev", {
				stream: {
					write: (info: string) => {
						info = info.replace(/(\r\n\t|\n|\r\t)/g, "");
						this.logger.info(info);
					},
				},
			}),
		);
	}

	public async start() {
		this.logger.info(`Socket server starting...`);

		this.app.use("*", (req, res) => {
			res.status(404).end();
		});

		this.socket.listen(this.options.port || 8080);
		this.logger.info(`Listening on port ${this.options.port || 8080}...`);
	}
}
