import * as express from "express";
import * as passport from "passport";

import { auth } from "./routes/auth";
import { quota } from "./routes/quota";
import { week } from "./routes/week";
import { Server } from "./Server";

interface RouteConfig {
	path: string;
	handle: express.RequestHandler | express.RequestHandler[];
}

export const routes = (server: Server): RouteConfig[] => {
	return [
		{
			handle: [
				passport.authenticate("bearer", { session: false }),
				quota,
			],
			path: "/users",
		},
		{
			handle: [passport.authenticate("bearer", { session: false }), week],
			path: "/week",
		},
		{
			handle: auth,
			path: "/oauth",
		},
	];
};
