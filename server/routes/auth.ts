import { AxiosResponse, default as axios } from "axios";
import { Router } from "express";

import { ClientUserModel } from "../../shared/models/ClientUserModel";
import { UserModel } from "../../shared/models/UserModel";
import { ClientUser } from "../../shared/structures/ClientUser";
import { CLIENT_SECRET } from "../config";
import { fetchUser } from "../discord/fetchUser";
import { fetchUserGuilds } from "../discord/fetchUserGuilds";
import { BadRequest } from "../errors/BadRequest";
import { Unauthorized } from "../errors/Unauthorized";

export const auth = Router()
	.use("/login", (req, res) => {
		res.redirect(
			"https://discordapp.com/api/oauth2/authorize?client_id=513837862832177202&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Foauth%2Ftoken&response_type=code&scope=identify%20guilds",
		);
	})
	.use("/token", async (req, res) => {
		if (!req.query.code) {
			return res.json(BadRequest());
		}
		let response: AxiosResponse<any> | undefined;
		try {
			response = await axios({
				data: `client_id=513837862832177202&client_secret=${CLIENT_SECRET}&code=${
					req.query.code
				}&redirect_uri=http://localhost:8080/oauth/token&grant_type=authorization_code&scope=identify guilds`,
				headers: {
					"Content-Type": "application/x-www-form-urlencoded",
				},
				method: "POST",
				url: `https://discordapp.com/api/oauth2/token`,
			});
		} catch (err) {
			console.error(err);
			res.json({ code: 2, message: "Server Error" }).status(500);
		}
		if (!response) {
			return;
		}
		const token = response.data.access_token;

		const user = await fetchUser(token);
		const guilds = await fetchUserGuilds(token);

		if (!guilds.find((v) => v.id === "360462032811851777")) {
			res.json(Unauthorized()).status(401);
		} else {
			await UserModel.update({ uid: user.uid }, user.toModel(), {
				upsert: true,
			});

			await ClientUserModel.update(
				{ uid: user.uid },
				new ClientUser({
					expiresAt: response.data.expires_in + Date.now(),
					token,
					uid: user.uid,
				}).toModel(),
				{ upsert: true },
			);

			return res.json({ token }).status(200);
		}
	});
