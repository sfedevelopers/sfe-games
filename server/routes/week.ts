import { Router } from "express";
import { existsSync, mkdirSync, readdirSync, readFileSync } from "fs";

export const week = Router().use("/:week", (req, res) => {
	const bin = `${__dirname}/../../app/bin`;
	const quotaDirectory = `${bin}/${req.params.week}`;

	if (!existsSync(bin)) {
		mkdirSync(bin);
	}

	if (!existsSync(quotaDirectory)) {
		return res.json({});
	}
	const entries: Array<{ id: string; msgs: number }> = [];

	readdirSync(quotaDirectory).map((v) => {
		const id = v.split(".json")[0];
		const msgs = JSON.parse(
			readFileSync(`${quotaDirectory}/${v}`).toString(),
		);
		entries.push({ id, msgs });
	});

	return res.json(entries).status(200);
});
