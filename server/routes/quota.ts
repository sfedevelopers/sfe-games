import { Router } from "express";
import { existsSync, mkdirSync, readdirSync, readFileSync } from "fs";

import { QuotaWeek } from "../../app/util/Util";

export const quota = Router()
	.use("/:id", (req, res) => {
		const date = new QuotaWeek();
		const bin = `${__dirname}/../../app/bin`;
		const quotaDirectory = `${bin}/${date.getFullYear()}-${date.getWeek()}`;
		const path = `${quotaDirectory}/${req.params.id}.json`;

		if (!existsSync(bin)) {
			mkdirSync(bin);
		}

		if (!existsSync(quotaDirectory)) {
			mkdirSync(quotaDirectory);
		}
		if (!existsSync(path)) {
			return res.json({});
		}
		const existingData = JSON.parse(readFileSync(path).toString());

		return res.json({ id: req.params.id, msgs: existingData });
	})
	.use("/", (req, res) => {
		const date = new QuotaWeek();
		const bin = `${__dirname}/../../app/bin`;
		const quotaDirectory = `${bin}/${date.getFullYear()}-${date.getWeek()}`;

		if (!existsSync(bin)) {
			mkdirSync(bin);
		}

		if (!existsSync(quotaDirectory)) {
			mkdirSync(quotaDirectory);
		}

		const entries: Array<{ id: string; msgs: number }> = [];

		readdirSync(quotaDirectory).map((v) => {
			const id = v.split(".json")[0];
			const msgs = JSON.parse(
				readFileSync(`${quotaDirectory}/${v}`).toString(),
			);
			entries.push({ id, msgs });
		});

		return res.json(entries).status(200);
	});
