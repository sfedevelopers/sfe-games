import Axios from "axios";

import { User } from "../../shared/structures/User";

export const fetchUser = async (token: string): Promise<User> => {
	try {
		const response = await Axios.get(
			`https://discordapp.com/api/users/@me`,
			{
				headers: { Authorization: "Bearer " + token },
			},
		);
		return new User(response.data);
	} catch (err) {
		console.error(err);
		throw err;
	}
};
