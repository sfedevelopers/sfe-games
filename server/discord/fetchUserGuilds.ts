import Axios from "axios";

import { DiscordAPIPartialGuild, PartialGuild } from "../../shared/structures/PartialGuild";

export const fetchUserGuilds = async (
	token: string,
): Promise<PartialGuild[]> => {
	try {
		const response = await Axios.get(
			`https://discordapp.com/api/users/@me/guilds`,
			{
				headers: { Authorization: "Bearer " + token },
			},
		);
		return response.data.map(
			(v: DiscordAPIPartialGuild) => new PartialGuild(v),
		);
	} catch (err) {
		console.error(err);
		throw err;
	}
};
