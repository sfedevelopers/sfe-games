import { default as chalk } from "chalk";
import * as express from "express";
import * as mongoose from "mongoose";
import * as morgan from "morgan";
import * as passport from "passport";
import { Strategy as BearerStrategy } from "passport-http-bearer";
import * as winston from "winston";

import { ClientUserModel } from "../shared/models/ClientUserModel";
import { NotFound } from "./errors/NotFound";
import { Unauthorized } from "./errors/Unauthorized";
import { routes } from "./routes";

interface ServerOptions {
	port?: number;
	debug?: boolean;
	dbURI?: string;
}

interface DefiniteServerOptions {
	port: number;
	debug: boolean;
	dbURI: string;
}

export class Server {
	public options: DefiniteServerOptions;

	public app: express.Application;
	public logger: winston.Logger;

	constructor(options?: ServerOptions) {
		this.options = Object.assign(
			{
				dbURI: "mongodb://localhost:27017/sfe-games",
				debug: false,
				port: 8080,
			},
			options,
		);

		this.app = express();
		this.logger = winston.createLogger({
			format: winston.format.combine(
				winston.format.colorize(),
				winston.format.timestamp(),
				winston.format.simple(),
				winston.format.printf((info) => {
					const { timestamp, level, message } = info;

					const ts = timestamp.slice(0, 19).split("T")[1];
					return `[${chalk.gray(ts)}] ${chalk.blue(
						"API",
					)} ${level} ${message}`;
				}),
			),
			transports: new winston.transports.Console({
				level: this.options.debug ? "debug" : "info",
			}),
		});

		this.app.use(
			morgan("dev", {
				stream: {
					write: (info: string) => {
						info = info.replace(/(\r\n\t|\n|\r\t)/g, "");
						this.logger.info(`[socket] ${info}`);
					},
				},
			}),
		);

		passport.use(
			new BearerStrategy((token, done) => {
				ClientUserModel.findOne({ token }, (err, res) => {
					if (err) {
						console.error(err);
						this.logger.error(err);
						return done(err, null);
					}
					if (!res) {
						return done(null, null);
					} else {
						if (res.expiresAt < Date.now()) {
							return done(Unauthorized(), null);
						} else {
							return done(null, res);
						}
					}
				});
			}),
		);

		routes(this).map((v) => this.app.use(v.path, v.handle));

		this.app.use("*", (req, res) => res.json(NotFound()).status(404));
	}
	public async start() {
		this.logger.info("Starting...");

		await mongoose
			.connect(this.options.dbURI, { useNewUrlParser: true })
			.catch((err) => {
				this.logger.error(err);
				process.exit(err);
			});

		this.app.listen(this.options.port);
		this.logger.info(`Listening on ${this.options.port}`);
	}
}
