export const NotFound = (n?: string) => {
	return {
		code: 0,
		message: `404: Not Found`,
	};
};
