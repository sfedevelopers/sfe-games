export const BadRequest = () => {
	return {
		code: 1,
		message: "400: Bad Request",
	};
};
