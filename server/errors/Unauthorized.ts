export const Unauthorized = () => {
	return {
		code: 3,
		message: "401: Unauthorized",
	};
};
