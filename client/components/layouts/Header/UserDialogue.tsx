import { Avatar, Badge, Dropdown, Icon, Menu } from "antd";
import SubMenu from "antd/lib/menu/SubMenu";
import * as React from "react";
import { Link } from "react-router-dom";

const UserSubMenu = () => (
	<Menu>
		<Menu.Item key="0">
			<Link to="/users/@me">
				<Icon type="user" />
				Profile
			</Link>
		</Menu.Item>
		<Menu.Item key="1">
			<Link to="/users/@me/settings">
				<Icon type="setting" />
				Settings
			</Link>
		</Menu.Item>
		<Menu.Divider />
		<Menu.Item key="3">
			<Icon type="logout" />
			Sign Out
		</Menu.Item>
		<Menu.Divider />
		<Menu.Item key="4" disabled>
			<Icon type="tool" />
			Super Secret Control Panel
		</Menu.Item>
	</Menu>
);

export class UserDialogue extends React.Component {
	public render() {
		return (
			<div className="layoutHeader-user">
				<Dropdown overlay={UserSubMenu}>
					<Badge>
						<Avatar shape="square" />
					</Badge>
				</Dropdown>
			</div>
		);
	}
}
