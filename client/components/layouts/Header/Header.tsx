import "./Header.less";

import { Layout, PageHeader } from "antd";
import * as React from "react";

import { LocationState } from "../../containers/LocationProvider";
import { LocationContext } from "../../contexts/LocationContext";
import { UserDialogue } from "./UserDialogue";

export const Header = (title: string) => {
	console.log("[HEADER] RENDER");
	return (
		<Layout.Header className="layoutHeader">
			<LocationContext.Consumer>
				{(loc: LocationState) => (
					<PageHeader
						className="layoutHeader-pageTitle"
						title={title || loc.pageTitle}
						onBack={
							loc.startLength < window.history.length
								? () => window.history.back()
								: undefined
						}
					/>
				)}
			</LocationContext.Consumer>

			<UserDialogue />
		</Layout.Header>
	);
};
