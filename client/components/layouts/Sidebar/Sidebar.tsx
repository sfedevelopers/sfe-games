import "./Sidebar.less";

import { Icon, Layout, Menu, Switch } from "antd";
import SubMenu from "antd/lib/menu/SubMenu";
import * as React from "react";
import { Link } from "react-router-dom";

export const Sidebar = () => (
	<Layout.Sider
		width={200}
		style={{ background: "#fff" }}
		className="layoutSider"
	>
		<div className="layoutSider-wrapper">
			<div className="layoutSider-logo">SFE GAMES</div>
			<Menu
				mode="inline"
				defaultSelectedKeys={["1"]}
				defaultOpenKeys={["sub1"]}
				style={{ borderRight: 0 }}
				className="layoutSider-nav"
			>
				<Menu.Item>
					<Link to="/users/@me">
						<span>
							<Icon type="user" />
							You
						</span>
					</Link>
				</Menu.Item>
				<Menu.Item>
					<Link to="/">
						<span>
							<Icon type="info" />
							Information
						</span>
					</Link>
				</Menu.Item>
				<Menu.Item>
					<Link to="/status">
						<span>
							<Icon type="setting" />
							Status
						</span>
					</Link>
				</Menu.Item>
				<Menu.Divider />
				<SubMenu
					key="sub3"
					title={
						<span>
							<Icon type="tool" />
							Utilities
						</span>
					}
				>
					<Menu.Item key="9">
						<Link to="/util/messages">Message Rates</Link>
					</Menu.Item>
					<Menu.Item key="10">
						<Link to="/util/joins">Join Rates</Link>
					</Menu.Item>
				</SubMenu>
			</Menu>
			<div className="layoutSider-themeToggler">
				<span>
					Toggle Theme <Switch size="small" />
				</span>
			</div>
		</div>
	</Layout.Sider>
);
