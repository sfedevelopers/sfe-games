import "./AppLayout.less";

import { Layout, PageHeader, Spin } from "antd";
import * as React from "react";
import { renderRoutes } from "react-router-config";

import { routes } from "../routes";
import { Header } from "./Header/Header";
import { Sidebar } from "./Sidebar/Sidebar";

export class AppLayout extends React.Component<any, { ready: boolean }> {
	constructor(props: any) {
		super(props);
		this.state = {
			ready: false,
		};

		setTimeout(() => this.setState({ ready: true }), 1e3);
	}
	public render() {
		return (
			<React.Fragment>
				<Layout>
					<Sidebar />
					<Layout>
						<Layout.Content className="layoutWrapper">
							{renderRoutes(routes)}
							<Layout.Footer className="layoutWrapper-footer">
								&copy; SFE Development Team 2019 &middot;{" "}
								<a href="https://twitter.com/sparklfox">
									sparklfox
								</a>
							</Layout.Footer>
						</Layout.Content>
					</Layout>
				</Layout>
				<div
					className="layoutLoader"
					style={{
						opacity: this.state.ready ? 0 : 1,
						zIndex: this.state.ready ? -1 : 1000,
					}}
				>
					<Spin
						size="large"
						tip="speen speen speen"
						wrapperClassName="layoutLoader"
					/>
				</div>
			</React.Fragment>
		);
	}
}
