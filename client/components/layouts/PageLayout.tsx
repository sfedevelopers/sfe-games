import { Layout } from "antd";
import * as React from "react";

import { Header } from "./Header/Header";

interface PageLayoutPage {
	component: JSX.Element;
	title: string;
}

export class PageLayout extends React.Component<{
	page: PageLayoutPage;
}> {
	public render() {
		return (
			<Layout.Content className="layoutWrapper">
				{Header(this.props.page.title)}
				<Layout.Content className="layoutWrapper-content">
					{this.props.page.component}
				</Layout.Content>
			</Layout.Content>
		);
	}
}
