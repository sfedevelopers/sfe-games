import * as React from "react";

export class Page extends React.Component<{ title: string }> {
	public render() {
		return (
			<div className={`page-${this.props.title}`}>
				{this.props.children}
			</div>
		);
	}
}
