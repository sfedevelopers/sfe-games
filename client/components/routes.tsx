import * as React from "react";
import { Route, Switch } from "react-router";
import { RouteConfig } from "react-router-config";

import { LocationContainer } from "./containers/LocationProvider";
import { UserContext } from "./contexts/UserContext";
import { AppLayout } from "./layouts/AppLayout";
import { PageLayout } from "./layouts/PageLayout";
import { InformationPage } from "./pages/InformationPage";
import { JoinRatePage } from "./pages/JoinRatePage";
import { MessageRatePage } from "./pages/MessageRatePage";
import { StatusPage } from "./pages/StatusPage";
import { UserPage } from "./pages/UserPage";

export const routes: RouteConfig[] = [
	{
		component: () => (
			<PageLayout
				page={{
					component: InformationPage,
					title: "Information",
				}}
			/>
		),
		exact: true,
		path: "/",
	},
	{
		component: () => (
			<PageLayout
				page={{
					component: StatusPage,
					title: "Status",
				}}
			/>
		),
		path: "/status",
	},
	{
		component: () => (
			<PageLayout
				page={{
					component: MessageRatePage,
					title: "Message Rates",
				}}
			/>
		),
		path: "/util/messages",
	},
	{
		component: () => (
			<PageLayout
				page={{
					component: JoinRatePage,
					title: "Join Rates",
				}}
			/>
		),
		path: "/util/joins",
	},
	{
		component: () => (
			<PageLayout
				page={{
					component: UserPage,
					title: "Loading...",
				}}
			/>
		),
		path: "/users/:id",
	},
];

export const route = (
	<Switch>
		<UserContext.Consumer>
			{(userConfig) =>
				userConfig.token ? <AppLayout /> : <h1>Not logged in</h1>
			}
		</UserContext.Consumer>
	</Switch>
);
