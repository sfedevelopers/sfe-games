import * as React from "react";

export const UserContext = React.createContext<{
	id: null | string;
	token: null | string;
}>({
	id: null,
	token: null,
});
