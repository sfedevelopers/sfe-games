import { Divider, Typography } from "antd";
import * as React from "react";

import { Page } from "../layouts/Page";

export const InformationPage = (
	<Page title="Status">
		<Typography>
			<Typography.Title level={3}>General</Typography.Title>
			<Typography.Paragraph>
				This bot is designed to cause maximum fun times in the{" "}
				<a href="https://discord.gg/sfe">SFE Discord Server.</a>
			</Typography.Paragraph>
			<Typography.Title level={4}>Features</Typography.Title>
			<Typography.Paragraph>
				<ul>
					<li>owo</li>
					<li>uwu</li>
					<li>xD</li>
					<li>rawr</li>
				</ul>
			</Typography.Paragraph>
			<Typography.Paragraph>
				Yeh, there aren't very many yet.
			</Typography.Paragraph>
			<Typography.Title level={3}>Work in Progress</Typography.Title>
			<Typography.Paragraph>
				Please keep in mind this bot isn't finished, and is using my
				half-built, 3 thirds functioning DJS framework since I like it
				slightly too much. Additionally, this bot is programmed in
				TypeScript -{" "}
				<b>you'll find an awful lot of standard JS does not work</b>, so
				please be careful when submitting merge requests, and
				contributions.
			</Typography.Paragraph>
			<Typography.Title level={3}>Setup</Typography.Title>
			Should only need to run <Typography.Text code>
				yarn
			</Typography.Text>{" "}
			and <Typography.Text code>yarn start</Typography.Text> if I've done
			everything right.
			<Typography.Paragraph />
			<Divider />
		</Typography>
	</Page>
);
