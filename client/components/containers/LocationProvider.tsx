import * as React from "react";

import { LocationContext } from "../contexts/LocationContext";

export interface LocationState {
	changeLocation: (path: string, pageTitle: string) => void;
	path: string;
	pageTitle: string;
	startLength: number;
}

export class LocationContainer extends React.Component<{}, LocationState> {
	public changeLocation: (path: string, pageTitle: string) => void;
	constructor(props: any) {
		super(props);

		this.changeLocation = (path: string, pageTitle: string) => {
			this.setState({
				changeLocation: this.changeLocation,
				pageTitle,
				path,
			});
		};

		this.state = {
			changeLocation: this.changeLocation,
			pageTitle: "Information",
			path: "/",
			startLength: window.history.length,
		};

		console.log("[LOCATION]", this.state.startLength);
	}

	public render() {
		return (
			<LocationContext.Provider value={this.state}>
				{this.props.children}
			</LocationContext.Provider>
		);
	}
}
