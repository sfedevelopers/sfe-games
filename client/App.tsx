import "./App.less";

import * as React from "react";
import { BrowserRouter } from "react-router-dom";

import { UserContext } from "./components/contexts/UserContext";
import { route } from "./components/routes";

export class App extends React.Component<
	any,
	{ userState: { id: string | null; token: string | null } }
> {
	public render() {
		return (
			<UserContext.Provider value={this.state.userState}>
				<BrowserRouter children={route} />
			</UserContext.Provider>
		);
	}
}
