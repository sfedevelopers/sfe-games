declare module "juration" {
	export function stringify(number: number): string;
	export function parse(string: string): number;
}
