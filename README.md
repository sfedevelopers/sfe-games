# SFE Games - bot for fun sthuff

This bot is designed to cause maximum fun times in the [SFE Discord Server](https://discord.gg/sfe).

## Work in Progress

Please keep in mind this bot isn't finished, and is using my half-built, 3 thirds functioning DJS framework since I like it slightly too much.

Additionally, this bot is programmed in TypeScript - **you'll find an awful lot of standard JS does not work**, so please be careful when submitting merge requests, and contributions.

## Setup

Should only need to run `yarn` and `yarn start` if I've done everything right.
